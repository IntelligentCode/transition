//
//  ReplaceFadeSegue.swift
//  Transfers
//
//  Created by Artem Shlyakhtin on 12/01/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit

class ReplaceFadeSegue: UIStoryboardSegue {
    
    override func perform() {
        let animation = UIApplication.shared.statusBarOrientationAnimationDuration
        
        let window = UIApplication.shared.keyWindow
        window?.insertSubview(destination.view, belowSubview: source.view)
        
        UIView.animate(withDuration: animation, animations: {
            self.source.view.layer.opacity = 0.0
        }) { (finished) in
            window?.rootViewController = self.destination
        }
    }
    
}
