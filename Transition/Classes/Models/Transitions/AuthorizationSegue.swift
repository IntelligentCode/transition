//
//  AuthorizationSegue.swift
//  Transfers
//
//  Created by Artem Shlyakhtin on 25/10/2016.
//  Copyright © 2016 Artem Shlyakhtin. All rights reserved.
//

import UIKit

class AuthorizationSegue: UIStoryboardSegue {
    
    override func perform() {
        guard let snapshot = source.view.snapshotView(afterScreenUpdates: true) else {
            UIApplication.shared.keyWindow?.rootViewController = self.destination
            return
        }
        
        let animation = UIApplication.shared.statusBarOrientationAnimationDuration
        let window = UIApplication.shared.keyWindow
        window?.addSubview(snapshot)
        window?.insertSubview(destination.view, belowSubview: snapshot)
        let transform = offset(snapshot.frame.height)
        
        UIView.animate(withDuration: animation, animations: {
            snapshot.layer.setAffineTransform(transform)
        }) { (finished) in
            window?.rootViewController = self.destination
            snapshot.removeFromSuperview()
        }
    }
    
    fileprivate func offset(_ value: CGFloat) -> CGAffineTransform {
        return CGAffineTransform(translationX: 0, y: value)
    }
    
}
