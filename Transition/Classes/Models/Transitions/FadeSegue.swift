//
//  FadeSegue.swift
//  Utilities
//
//  Created by Artem Shlyakhtin on 02.09.16.
//  Copyright © 2016 Artem Shlyakhtin. All rights reserved.
//

import UIKit

class FadeSegue: UIStoryboardSegue {
    
    override func perform() {
        let transition: CATransition = CATransition()
        transition.type = kCATransitionFade
        source.view.window?.layer.add(transition, forKey: "kCATransition")
        source.navigationController?.pushViewController(destination, animated: false)
    }

}
